def getsum(n):
    sum = 0
    for i in range(0, n, 3):
        sum += i
    for i in range(0, n, 5):
        sum += i
    for i in range(0, n, 15):
        sum -= i
    return sum

if __name__ == '__main__' :
    sum = getsum(1000)
    print(sum)
